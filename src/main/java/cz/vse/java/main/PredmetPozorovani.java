package cz.vse.java.main;

public interface PredmetPozorovani {

    /**
     * Metoda, kterou použije pozorovatel, aby se přihlásil k odběru změn (volání metody aktulizuj)
     * @param pozorovatel
     */
    void registruj(Pozorovatel pozorovatel);

}
