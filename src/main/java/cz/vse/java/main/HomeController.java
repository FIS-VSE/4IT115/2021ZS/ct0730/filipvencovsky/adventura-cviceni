package cz.vse.java.main;

import cz.vse.java.logika.Hra;
import cz.vse.java.logika.IHra;
import cz.vse.java.logika.PrikazJdi;
import cz.vse.java.logika.Prostor;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Point2D;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.util.HashMap;
import java.util.Map;

public class HomeController implements Pozorovatel {

    private IHra hra;
    @FXML
    private TextArea vystup;
    @FXML
    private TextField vstup;
    @FXML
    private Button odesli;
    @FXML
    private ListView<Prostor> seznamVychodu;
    @FXML
    private ImageView hrac;

    @FXML
    public void initialize() {
        hra = new Hra();
        hra.getHerniPlan().registruj(this);
        vystup.setText(hra.vratUvitani()+"\n\n");
        nacteniMistnosti();
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                vstup.requestFocus();
            }
        });

        seznamVychodu.setCellFactory(new Callback<ListView<Prostor>, ListCell<Prostor>>() {
            @Override
            public ListCell<Prostor> call(ListView<Prostor> param) {
                return new ListCell<Prostor>() {
                    @Override
                    protected void updateItem(Prostor prostor, boolean b) {
                        super.updateItem(prostor,b);
                        if(prostor!=null) {
                            ImageView obrazek = new ImageView("hrac.png");
                            obrazek.setFitHeight(60);
                            obrazek.setPreserveRatio(true);
                            setText(prostor.getNazev());
                            setGraphic(obrazek);
                        } else {
                            setText("");
                            setGraphic(new ImageView());
                        }
                    }
                };
            }
        });
    }

    @FXML
    private void zobrazNapovedu() {
        Stage stage = new Stage();

        WebView view = new WebView();
        view.getEngine().load(getClass().getResource("/napoveda.html").toExternalForm());

        Scene scene = new Scene(view);
        stage.setScene(scene);
        stage.show();
    }

    private static Map<String, Point2D> getSouradniceProstoru() {
        Map<String, Point2D> map = new HashMap<>();
        map.put("domeček", new Point2D(0,62));
        map.put("les", new Point2D(63,24));
        map.put("hluboký_les", new Point2D(134,62));
        map.put("jeskyně", new Point2D(136,124));
        map.put("chaloupka", new Point2D(193,14));
        return map;
    }

    private void nacteniMistnosti() {
        Prostor aktualniProstor = hra.getHerniPlan().getAktualniProstor();
        seznamVychodu.getItems().addAll(aktualniProstor.getVychody());
        Point2D souradnice = getSouradniceProstoru().get(aktualniProstor.getNazev());
        hrac.setLayoutX(souradnice.getX());
        hrac.setLayoutY(souradnice.getY());
    }

    public void zpracujVstup(ActionEvent actionEvent) {
        String prikaz = vstup.getText();
        zpracujPrikaz(prikaz);
    }

    private void zpracujPrikaz(String prikaz) {
        String vysledek = hra.zpracujPrikaz(prikaz);
        vystup.appendText("> "+prikaz+"\n"+vysledek+"\n\n");
        vstup.setText("");

        if(hra.konecHry()) {
            vystup.appendText(hra.vratEpilog());
            vstup.setDisable(true);
            odesli.setDisable(true);
            seznamVychodu.setDisable(true);
        }
    }

    @Override
    public void aktualizuj() {
        seznamVychodu.getItems().clear();
        nacteniMistnosti();
    }

    public void klikSeznamVychodu(MouseEvent mouseEvent) {
        Prostor novyProstor = seznamVychodu.getSelectionModel().getSelectedItem();
        if(novyProstor==null) return;
        String prikaz = PrikazJdi.NAZEV+" "+novyProstor;
        zpracujPrikaz(prikaz);
    }
}
